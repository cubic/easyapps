<?php
namespace User\Controller;
use Think\Controller;
class PublicController extends Controller {
    

    public function ckLogin()
    {
        $account = I('account');
        $password = I('password');

        unset($map);
        $map['account'] = $account;
        $map['password'] = md5($password);
        $Member = M('Member');
        $user  = $Member->where($map)->find();
        if (!$user) {
            $this->error('登录帐号或密码错误');
            return;
        }

        unset($map);
        $map['id'] = $user['id'];

        //先将数据库里面的本次登录IP改为上次登录IP
        if ($user['login_ip'] && $user['login_time']) {
            $data['last_ip'] = $user['login_ip'];
            $data['last_time'] = $user['login_time'];
            $Member->where($map)->save($data);
        }
        unset($data);
        $data['login_ip'] = get_client_ip();
        $data['login_time'] = time();
        $Member->where($map)->save($data);
        $Member->where($map)->setInc('logins');
        session('uid',$user['id']);
        session('account',$user['account']);
        session('name',$user['name']);
        $this->success('恭喜你，登录成功','index.php');
    }

    public function logout()
    {
        session('uid',null);
        // session('rid',null);
        session('account',null);
        redirect('index.php');
    }
}